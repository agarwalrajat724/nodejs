A Callback function is a function that gets passed as a argument to another function and
is executed after some event happens.