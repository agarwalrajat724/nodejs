const fs = require('fs');
const os = require('os');
const notes = require('./notes');
const _ = require('lodash');
const yargs = require('yargs');

console.log("Process : " , process.argv);

const argv = yargs.argv;
// var command = process.argv[2];

var command = argv._[0];

console.log(" Yargs : " , argv );

console.log(`Command : ${command}`);

if(command  === 'add') {
    var note = notes.addNote(argv.title, argv.body);
    if(note === undefined) {
        console.log('Note already exists');
    } else {
        console.log('Added Note : ', note);
    }
} else if (command === 'list') {
    let notesArray = notes.getAll();
    if(notesArray.length > 0) {
        notesArray.forEach((note) => {
            console.log(note);
        })
    };
} else if (command === 'read') {
    var note = notes.getNote(argv.title);
    if(note) {
        console.log('Read Note : ', note);
    } else {
        console.log('Node Not Found.....');
    }
} else if(command === 'remove') {
    var noteRemoved = notes.removeNote(argv.title);
    noteRemoved ? console.log('Note Removed : ', argv.title) : console.log('Note not Found....');
} else {
    console.log(`COmmand not Recognised`);
}

// For Debugging start the file with node inspect filename.js
// n to go to next line
// c to continue 
// repl to do various operation on the cli 

// To debug via Chrome-Developer Tools
// node --inspect-brk playground/json.js

// console.log(_.isString('abc'));
// console.log(_.isString(1));

// let arr = _.uniq([2,2,1,1,11111,1,1,1]);
// console.log(arr);


// var res = notes.addNote();
// console.log(res);

// console.log(notes.add(9, 5));

//console.log("Age : " + notes.age);


//let user = os.userInfo();
//console.log(user);

//fs.appendFile('greetings.txt', `Hello ${user.username}!!!\n`);

// For Node version 7 or more

// fs.appendFile('greetings.txt', 'Hello World 7_1!!\n', function(err) {
//     if(err) {
//         console.log('Unable to Append to File.... ');
//     }
// });

// fs.appendFileSync('greetings.txt', 'Hello World 7_2 !!!\n');

// Inside all our node files we have access to a variable called module


