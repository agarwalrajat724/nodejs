const fs = require('fs');


var fetchNotes = () => {
    debugger;
    try {
        var noteString = fs.readFileSync('notes-data.json');
        return JSON.parse(noteString);
    } catch(ex) {
        console.log("Exception...... ");
        return [];
    }
};

var saveNotes = (notes) => {
    fs.writeFileSync('notes-data.json', JSON.stringify(notes));
};

var addNote = (title, body) => {
    var notes = fetchNotes();
    var note = {
        title,
        body
    };

    // var duplicateNotes = notes.filter((note) => {
    //     return note.title === title;
    // })

    // OR

    var duplicateNotes = notes.filter((note) => note.title === title);

    if (duplicateNotes.length === 0) {
        notes.push(note);
        saveNotes(notes);
        return note;
    }

};

var getAll = () => {
    return fetchNotes();
}

var getNote = (title) => {
    var notes = fetchNotes();

    var filteredNote = notes.filter((note) => {
        return note.title === title;
    });

    return filteredNote[0];
}

var removeNote = (title) => {
    var notes = fetchNotes();
    var filteredNotes = notes.filter((note) => note.title !== title);
    saveNotes(filteredNotes);

    return notes.length !== filteredNotes.length;
}


module.exports = {
    // addNote: addNote  OR
    addNote,
    getAll,
    getNote,
    removeNote
}

// console.log(module);

// module.exports.age = 25;

// The big difference between function() {} and Arrow functions is that the Arrow function is
// not going to bind the this keyword or the arguments array

// module.exports.addNote = () => {
//     console.log(`Add Note`);
//     return 'New Note';
// };


// module.exports.add = (a, b) => {
//     return a+b;
// }