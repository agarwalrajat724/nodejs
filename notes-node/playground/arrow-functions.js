var square = (x) => {
    var result = x * x;
    return result;
}; 

console.log(square(3));


var square1 = (x) => x * x;

console.log(square1(9));


var square2 = x => x*x;

console.log(square2(10));

// Arrow Function do not bind a this Keyword

var user = {
    name: 'Rajat',
    sayHi: () => {
        console.log(`Hi !! ${this.name}`);
    }
};

user.sayHi();


var user1 = {
    name: 'Rajat',
    sayHi: () => {
        console.log(`Hi !! ${this.name}`);
    },

    sayHiAlt() {
        console.log(arguments);
        console.log(`Hi I'm ${this.name}`);
    }
};

user1.sayHi();
user1.sayHiAlt();


user1.sayHiAlt(1,2,3,4); 
// Produces below o/p
// { '0': 1, '1': 2, '2': 3, '3': 4 }
//Hi I'm Rajat


// In General when we don't need the this keyword or the arguments keyword 
// we can use arrow function with object

