// var obj = {
//     name: 'Rajat'
// };

// var stringObj = JSON.stringify(obj);

// console.log(typeof stringObj);
// console.log(stringObj);


// var personString = '{"name": "Rajat", "age": 25}';

// var personObj = JSON.parse(personString);
// console.log(typeof personObj);
// console.log(personObj);
// console.log(personObj.name);
// console.log(personObj.age);

const fs = require('fs');

let originalNote = {
    title: 'Some title',
    body: 'Some body'
};

// originalNoteString
let originalNoteString = JSON.stringify(originalNote);

fs.writeFileSync('notes.json', originalNoteString);

var noteString = fs.readFileSync('notes.json');
var note = JSON.parse(noteString);
console.log(typeof note);
console.log(note.title);
