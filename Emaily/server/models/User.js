const mongoose = require('mongoose');
// const Schema = mongoose.Schema;
// The above statement can also be written as

const { Schema } = mongoose;

const userSchema = new Schema({
    googleId: String
});

mongoose.model('users', userSchema);