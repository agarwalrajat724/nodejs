const passport = require('passport');

module.exports = app => {
    
    app.get(
        '/auth/google', 
        passport.authenticate('google', {
            scope: ['profile', 'email']
        })
    );

    app.get(
        '/auth/google/callback',
        passport.authenticate('google')
    );


    app.get('/api/logout', (req, res) => {
        // It takes the Cookie that contains our User Id and it kills 
        // the Id that's in there and says OK u r logged out
        req.logout();
        res.send(req.user);
    })

    app.get('/api/test', (req, res) => {
        res.send(req.user);
    });

}